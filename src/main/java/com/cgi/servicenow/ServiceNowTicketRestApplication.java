package com.cgi.servicenow;

import com.cgi.servicenow.persistence.entities.Ticket;
import com.cgi.servicenow.persistence.service.TicketService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Optional;

@SpringBootApplication
public class ServiceNowTicketRestApplication {

	public static void main(String[] args) {

		SpringApplication.run(ServiceNowTicketRestApplication.class, args);

//		AnnotationConfigApplicationContext annotationConfigApplicationContext =
//				new AnnotationConfigApplicationContext(ServiceNowTicketRestApplication.class);
//
//
//		TicketService ticketService = annotationConfigApplicationContext.getBean(TicketService.class);
//
//		Optional<Ticket> ticket =  ticketService.getTicketById(1L);
//
//		System.out.println(ticket.get());

	}
}
