package com.cgi.servicenow.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.cgi.servicenow.persistence.entities.Ticket;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Optional <Ticket> findByIdTicket(Long id_ticket);

    Optional <Ticket> findByName(String name);

}
