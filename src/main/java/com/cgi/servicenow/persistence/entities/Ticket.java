package com.cgi.servicenow.persistence.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Table(name = "ticketTestTableService")
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ticket")
	private Long idTicket;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "id_person_creator", nullable = false)
	private Long idPersonCreator;

	@Column(name = "id_person_assigned", nullable = false)
	private Long idPersonAssigned;

	@Column(name = "creation_datetime", nullable = false)
	private LocalDate creationDatetime;

	public Ticket() {
	}

	public Ticket(Long idTicket, String name, String email, long idPersonCreator, long idPersonAssigned, LocalDate creationDatetime) {
		this.idTicket = idTicket;
		this.name = name;
		this.email = email;
		this.idPersonCreator = idPersonCreator;
		this.idPersonAssigned = idPersonAssigned;
		this.creationDatetime = creationDatetime;
	}

	public Long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(Long idTicket) {
		this.idTicket = idTicket;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIdPersonCreator() {
		return idPersonCreator;
	}

	public void setIdPersonCreator(Long idPersonCreator) {
		this.idPersonCreator = idPersonCreator;
	}

	public Long getIdPersonAssigned() {
		return idPersonAssigned;
	}

	public void setIdPersonAssigned(Long idPersonAssigned) {
		this.idPersonAssigned = idPersonAssigned;
	}

	public LocalDate getCreationDatetime() {
		return creationDatetime;
	}

	public void setCreationDatetime(LocalDate creationDatetime) {
		this.creationDatetime = creationDatetime;
	}

	@Override
	public String toString() {
		return "Ticket{" +
				"idTicket=" + idTicket +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", idPersonCreator=" + idPersonCreator +
				", idPersonAssigned=" + idPersonAssigned +
				", creationDatetime=" + creationDatetime +
				'}';
	}
}
