package com.cgi.servicenow.persistence.service;

import com.cgi.servicenow.persistence.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cgi.servicenow.persistence.entities.Ticket;

import java.util.Optional;


@Service
public class TicketService {

	private TicketRepository ticketRepository;

	@Autowired
	public TicketService(TicketRepository ticketRepository) {
		this.ticketRepository = ticketRepository;
	}

	//CREATE TICKET
	public Ticket createTicket(Ticket ticket) {
		System.out.println("TicketService receive the ticket - " + ticket);
		return ticketRepository.save(ticket);
	}


	// GET ALL TICKETS
	public Iterable<Ticket> getAllTickets() {
		return ticketRepository.findAll();
	}


	//GET TICKET BY ID
	public Optional<Ticket> getTicketById(Long ticketId) {
			return ticketRepository.findByIdTicket(ticketId);
		//return null;
	}

	//GET TICKET BY NAME
	public Optional<Ticket> getTicketByName(String name) {
		return ticketRepository.findByName(name);
	}


}
