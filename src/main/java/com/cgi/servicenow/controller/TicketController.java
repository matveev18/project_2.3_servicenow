package com.cgi.servicenow.controller;

import com.cgi.servicenow.persistence.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.cgi.servicenow.persistence.entities.Ticket;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value="/api")
public class TicketController {

    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    //CREATE TICKET
    @PostMapping(value="/create")
    public Ticket createTicket(@RequestBody Ticket ticket){
        System.out.println("Controller receive the ticket - " + ticket);
       // return
                ticketService.createTicket(ticket);
return null;
/*
        @PostMapping("/students")
        public ResponseEntity<Object> createStudent(@RequestBody Student student) {
            Student savedStudent = studentRepository.save(student);

            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(savedStudent.getId()).toUri();

            return ResponseEntity.created(location).build();

        }
*/


    }

    // GET ALL TICKETS
    @GetMapping(value = "/alltickets")
    public Iterable<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }


    //GET TICKET BY ID
    @GetMapping(value="/ticketId/{ticketId}")
    public Optional<Ticket> getTicketById(@PathVariable("ticketId")Long ticketId){
        return ticketService.getTicketById(ticketId);
    }


    //GET TICKET BY NAME
    @GetMapping(value="/ticketName/{name}")
    public Optional<Ticket> getTicketByName(@PathVariable("name")String name){
        return ticketService.getTicketByName(name);
    }


}


